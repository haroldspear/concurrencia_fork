#include "csapp.h"
#include <stdlib.h>


void echo(int connfd);
void sigchld_handler(int sig){
	while (waitpid(-1, 0, WNOHANG) > 0)
	;
	return;
}

int main(int argc, char **argv)
{
	int listenfd, connfd;
	
	//unsigned int clientlen;
	socklen_t clientlen;
	struct sockaddr_storage clientaddr;
	//struct sockaddr_in clientaddr;
	//struct hostent *hp;
	char *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	Signal(SIGCHLD, sigchld_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(struct sockaddr_storage);
		connfd = Accept(listenfd, (SA *) &clientaddr,&clientlen);
		if(Fork() == 0){//se usa Fork() y luego si el proceso que llega es el padre se cerrarà mientras que si es el hijo usará echo, entonces como siempre un padre estará saltandose el if concurrentemente(mientras se ejecuta la función echo del child), se tendrán varios child escuchando clientes.
			Close(listenfd);
			echo(connfd);
			Close (connfd);
			exit (0);
		}
		Close(connfd);
	}
}

void echo(int connfd)
{
		
	size_t n;
	char buf[MAXLINE];
	rio_t rio;
	/*
	char *pbuf;
	char *pc;
	pbuf=buf;
	pc=buf;
	int countc=0;
	int count=0;
	*/
	char fileName[MAXLINE];
	char command[MAXLINE];
	strcpy(fileName,"./scripts/");
	Rio_readinitb(&rio, connfd);
	int rfa;
	int fd;
	int fdd;
	char fileError[14];
	char bufff[2];
	strcpy(fileError,"./errores.txt");
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		strtok(buf,"\n");		
		strcat(fileName, buf);
		strtok(fileName," ");
		strcpy(command, buf);
		printf("Server will try executing command: %s\n", command);
		/* Para direccionar a un ejecutable por ejemplo /bin/ls, si necesitaba el siguiente código sacar cada parametro y ponerlo por separado en el arreglo arg

		while(*pc){//contar los espacios en blanco para calcular el tamaño del arreglo de parámetros
			while(*pc && *pc !=' ')pc++;
			if(!*pc){
				break;
			}		
			pc++;
			countc++;
		}
		char *arg[countc+2];//2 extra porque el primer valor se setea "", porque en ocuaciones no lee el primer paràmetro, pero si los siguientes y el ultimo NULL
		while(*pbuf){//poner cada palabra en un arreglo exceptuando el comando, quedarìa el arreglo de parámetros, ejemplo de la sentencia ls -l -s -a, quedará el arreglo {"","-l","-s","-a",(NULL)}
			if (count>0){
				arg[count]=pbuf;
			}
			if(count==0){
				arg[count]="";
			}
			while(*pbuf && *pbuf !=' ')pbuf++;
			if(!*pbuf){
				break;
			}	
			*pbuf=0;
			pbuf++;
			count++;	
		}

		count++;
		arg[count]=NULL;
		*/
		char *arg[1];
		arg[0]=NULL;
		char buff[MAXLINE];
		strncpy(buff,"#!/bin/bash\n",13);
		remove(fileName);
		fd=open(fileName, O_CREAT|O_WRONLY|O_APPEND,S_IRWXO|S_IRWXU|S_IRWXG);
		rfa=write(fd,buff,strlen(buff));
		strcpy(buff,command);				
		rfa=write(fd,command,strlen(command));
		strncpy(buff,"\nif [ $? -ne 0 ]; then\n",25);//Añado al script el manejo de errores
		rfa=write(fd,buff,strlen(buff));
		remove(fileError);
		strncpy(buff,"echo \"-1\">> ./errores.txt\n",27);
		rfa=write(fd,buff,strlen(buff));
		strcpy(buff,"fi");
		rfa=write(fd,buff,strlen(buff));		
		close(fd);
		
		
		if(rfa!=0){
			rfa=0;
			fd=0;	
		}
		int *exe = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE,MAP_SHARED | MAP_ANONYMOUS, -1, 0);//uso memoria compartida
		*exe=99;
		if(fork()==0){//Se aplica concurrencia para que execv no cierre un proceso
			
			*exe=3;			
			*exe=execv(fileName,arg);
			exit(0);		
			
				
		}else{
			wait(NULL);
			fdd=open("./errores.txt", O_RDONLY);
			rfa=read(fdd,bufff,2);
			close(fdd);					
			if(*exe==-1 || (strcmp(bufff,"-1")==0)){
				strcpy(buf,"ERROR!\n");
				Rio_writen(connfd,buf,strlen(buf));	
			}else{
				strcpy(buf,"OK!\n");
				Rio_writen(connfd,buf,strlen(buf));	
			}
		}
	
		exit(0);
		munmap(exe, sizeof(int));			
	
	}
}
